from __future__ import annotations
from abc import ABC, abstractmethod
from random import choice, choices
from typing import List

"""Напишите консольную программу Бинго, где есть 5 игроков.
У игрока есть имя и билетик с 5 случайными числами( числа от 1 до 99).
Программа должна случайно выбирать число от 1 до 99,
выводить на консоль и оповещать игроков об этом числе.
Игрок, который первым собрал свою комбинацию, должен выкрикнуть свое имя и программа останавливается.
Для выполнения задания, используйте пожалуйста Observer pattern.
Можете использовать дополнительно другие паттерны по желанию"""

number_list = [i for i in range(1, 100)]

class Subject(ABC):
    @abstractmethod
    def attach(self, observer: Observer):
        pass

    @abstractmethod
    def detach(self, observer: Observer) -> None:
        pass

    @abstractmethod
    def notify(self) -> None:
        pass


class ConcreteSubject(Subject):

    game_list = []
    _observers: List[Observer] = []

    def attach(self, observer: Observer) -> None:
        self._observers.append(observer)

    def detach(self, observer: Observer) -> None:
        self._observers.remove(observer)

    def notify(self, game_choice) -> None:

        for observer in self._observers:
            self.game_list.append(game_choice)
            observer.update(self, self.game_list)


class Observer(ABC):

    @abstractmethod
    def update(self, subject: Subject) -> None:
        pass


class PlayerA(Observer):

    player_choice = choices(number_list, k=5)

    player_set = set(player_choice)
    player_name = "Ricardo Milos"
    print(f"Player: {player_name}, player number set: {player_set}")

    def update(self, subject: Subject, game_list) -> None:
        if self.player_set.issubset(game_list):
            print(f"{self.player_name} BINGO!!!")
            subject.detach(observer_a)
            exit(0)

class PlayerB(Observer):

    player_choice = choices(number_list, k=5)

    player_set = set(player_choice)
    player_name = "Elon Musk"
    print(f"Player: {player_name}, player number set: {player_set}")

    def update(self, subject: Subject, game_list) -> None:
        if self.player_set.issubset(game_list):
            print(f"{self.player_name} BINGO!!!")
            subject.detach(observer_b)
            exit(0)

class PlayerC(Observer):

    player_choice = choices(number_list, k=5)

    player_set = set(player_choice)
    player_name = "Guido van Rossum"
    print(f"Player: {player_name}, player number set: {player_set}")

    def update(self, subject: Subject, game_list) -> None:
        if self.player_set.issubset(game_list):
            print(f"{self.player_name} BINGO!!!")
            subject.detach(observer_c)
            exit(0)


class PlayerD(Observer):

    player_choice = choices(number_list, k=5)

    player_set = set(player_choice)
    player_name = "Pavel Durov"
    print(f"Player: {player_name}, player number set : {player_set}")

    def update(self, subject: Subject, game_list) -> None:
        if self.player_set.issubset(game_list):
            print(f"{self.player_name} BINGO!!!")
            subject.detach(observer_d)
            exit(0)


class PlayerE(Observer):

    player_choice = choices(number_list, k=5)
    player_set = set(player_choice)
    player_name = "Donald Ervin Knuth"
    print(f"Player: {player_name}, player number set: {player_set}")

    def update(self, subject: Subject, game_list) -> None:
        if self.player_set.issubset(game_list):
            print(f"{self.player_name} BINGO!!!")
            subject.detach(observer_e)
            exit(0)


if __name__ == '__main__':

    subject = ConcreteSubject()
    observer_a = PlayerA()
    observer_b = PlayerB()
    observer_c = PlayerC()
    observer_d = PlayerD()
    observer_e = PlayerE()
    subject.attach(observer_a)
    subject.attach(observer_b)
    subject.attach(observer_c)
    subject.attach(observer_d)
    subject.attach(observer_e)

    while True:
        game_choice = choice(number_list)
        print(f"We got number: {game_choice}")
        subject.notify(game_choice)